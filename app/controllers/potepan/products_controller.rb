class Potepan::ProductsController < ApplicationController
  DISPLAY_RELATED_PRODUCTS_NUMBER = 4

  def show
    @product = Spree::Product.find(params[:id])
    @related_products = @product.related_products.sample(DISPLAY_RELATED_PRODUCTS_NUMBER)
  end
end
