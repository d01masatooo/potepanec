require 'rails_helper'

RSpec.feature "Products", type: :feature do
  given(:product) { create(:product, taxons: [taxon]) }
  given(:taxonomy) { create(:taxonomy) }
  given(:taxon) { create(:taxon, taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  given!(:related_product) { create(:product, taxons: [taxon]) }
  given!(:unrelated_product) { create(:product) }

  background do
    visit potepan_product_path(product.id)
  end

  scenario "商品詳細ページが表示される" do
    expect(page).to have_current_path(potepan_product_path(product.id))
    within(".media-body") do
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
      expect(page).to have_content product.description
    end
  end

  scenario "一覧へ戻るをクリックすると、その商品のカテゴリー一覧ページが表示される" do
    click_on "一覧ページへ戻る"
    expect(page).to have_current_path(potepan_category_path(taxon.id))
  end

  scenario "関連商品を表示する" do
    within(".productBox") do
      expect(page).to have_content related_product.name
      expect(page).to have_content related_product.display_price
    end
  end

  feature "関連商品の表示数のテスト" do
    given!(:related_product) { create_list(:product, 5, taxons: [taxon]) }
    scenario "関連商品を4件表示する" do
      expect(page).to have_selector ".productBox", count: 4
    end
  end

  scenario "関連しない商品は表示しない" do
    within(".productBox") do
      expect(page).not_to have_content unrelated_product.name
    end
  end

  scenario "関連商品をクリックすると、その商品の詳細ページを表示する" do
    within(".productBox") do
      click_on related_product.name
      expect(page).to have_current_path(potepan_product_path(related_product.id))
    end
  end
end
