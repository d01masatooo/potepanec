require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  given(:taxonomy) { create(:taxonomy, name: "Categories") }
  given(:parent_taxon) { create(:taxon, name: "Brand", parent: taxonomy.root) }
  given(:child_taxon) { create(:taxon, name: "Bags", parent: parent_taxon) }
  given!(:product) { create(:product, taxons: [child_taxon]) }

  background do
    visit potepan_category_path(child_taxon.id)
  end

  scenario "商品カテゴリーが表示される" do
    expect(page).to have_current_path(potepan_category_path(child_taxon.id))
    expect(page).to have_title child_taxon.name
    within(".side-nav") do
      expect(page).to have_content taxonomy.name
      expect(page).to have_content child_taxon.name
      expect(page).to have_content parent_taxon.name
    end
  end

  scenario "商品カテゴリーをクリックすると、そのカテゴリーの商品一覧が表示される" do
    click_link child_taxon.name
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
  end

  scenario "商品をクリックすると、その商品詳細ページが表示される" do
    click_link product.name
    expect(page).to have_current_path(potepan_product_path(product.id))
  end
end
