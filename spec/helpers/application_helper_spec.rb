require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "full_titile method" do
    context "引数が設定されていない場合" do
      it "base_titleを返す" do
        expect(full_title).to eq "potepanec"
      end
    end

    context "引数が設定されている場合" do
      it "page_titleとbase_titleを合わせたタイトルを返す" do
        expect(full_title("example")).to eq "example | potepanec"
      end
    end
  end
end
