require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "#show" do
    let!(:taxon) { create(:taxon) }
    let!(:other_taxon) { create(:taxon) }
    let!(:product) { create(:product) }
    let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }
    let!(:related_product) { create(:product, taxons: [taxon]) }
    let!(:unrelated_product) { create(:product, taxons: [other_taxon]) }

    before do
      get :show, params: { id: product.id }
    end

    it "productが期待した値を持つ" do
      expect(assigns(:product)).to eq product
    end

    it "レスポンスに成功する" do
      expect(response).to be_successful
    end

    it "200レスポンス返すこと" do
      expect(response).to have_http_status "200"
    end

    it "showテンプレートを返すこと" do
      expect(response).to render_template :show
    end

    it "@related_productsは、関連商品商品を４つもつ" do
      expect(assigns(:related_products).size).to eq 4
    end

    it "@related_productsは、関連商品のみもつ" do
      expect(assigns(:related_products)).not_to include(unrelated_product)
    end
  end
end
