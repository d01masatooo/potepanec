require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "#show" do
    let(:taxonomy) { create(:taxonomy) }
    let(:parent_taxon) { create(:taxon, taxonomy: taxonomy, parent: taxonomy.root) }
    let(:child_taxon) { create(:taxon, taxonomy: taxonomy, parent: parent_taxon) }
    let!(:product_1) { create(:product, name: "同カテゴリーの商品", taxons: [child_taxon]) }
    let!(:product_2) { create(:product, name: "異カテゴリーの商品") }

    before do
      get :show, params: { id: child_taxon.id }
    end

    it "正常にレスポンスを返すこと" do
      expect(response).to be_successful
    end

    it "showテンプレートを返すこと" do
      expect(response).to render_template :show
    end

    it "taxonomyを表示する" do
      expect(assigns(:taxonomies)).to contain_exactly(taxonomy)
    end

    it "taxonを表示する" do
      expect(assigns(:taxon)).to eq(child_taxon)
    end

    it "同カテゴリーの商品を表示する" do
      expect(assigns(:products)).to contain_exactly(product_1)
    end

    it "異カテゴリーの商品は表示しない" do
      expect(assigns(:products)).not_to include(product_2)
    end
  end
end
